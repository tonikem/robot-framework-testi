# Robot Framework testi



## Projektin tarkoitus

Projektin tarkoituksena on testata robot framework -työkalua. Kohdesovellus on yksinkertainen nettikauppa, jota varten täytyy luoda käyttäjätunnus.

## Kuinka sovellusta testataan

Sovellusta testataan ensin luomalla käyttäjätunnus, jonka jälkeen tunnuksella kirjaudutaan sisään sovellukseen. Tämän jälkeen sovellus antaa lisätä tavaroita ostoskoriin. Lisätään tavaroita ostoskoriin testin aikana ja tarkistetaan aina välisumman oikeellisuus. Lopuksi tyhjennetään kori.

Sovelluksen testaus näkyy videolla: https://streamable.com/4zi5qu

