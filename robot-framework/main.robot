*** Settings ***
Resource  common.resource

*** Test Cases ***
Kirjautuminen ja tuotteiden osto
    Aloita ajo
    Luo uusi kayttaja  registration
    Page Should Contain  User has been registered successfully
    Kirjaudu uudella kayttajalla  login
    Osta tuotteita
    Poista Tuote ostoskorista  Soap
    Osta tuotteet
    Kirjaudu ulos

Saman tunnuksen luonti
    Luo uusi kayttaja  registration
    Varmista käyttäjän luonnin virheilmoitukset

Väärä Kirjautuminen
    Kirjautuminen väärillä tiedoilla  login
    Varmista kirjautumisen virheilmoitukset
    Sulje sovellus

